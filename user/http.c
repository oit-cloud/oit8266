#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <mem.h>
#include <espfs.h>
#include <osapi.h>
#include "eeprom.h"

#include "platform.h"
#include "httpd.h"
#include "httpdespfs.h"
#include "cgiwifi.h"
#include "cgiflash.h"
#include "auth.h"
#include "espfs.h"
#include "captdns.h"
#include "webpages-espfs.h"
#include "cgiwebsocket.h"

#include <oit.h>

#include "httpd.h"

static long hitCounter=0;
os_timer_t system_reset_timer;

int ICACHE_FLASH_ATTR cgiWiFiConnectHandler(HttpdConnData *connData) {
	char essid[128];
	char password[128];

  os_printf("POST\n");

	if (connData->conn==NULL) {
		//Connection aborted. Clean up.
		return HTTPD_CGI_DONE;
	}

  WifiConfig *wifiConfig = (WifiConfig*)os_zalloc(sizeof(WifiConfig));

	httpdFindArg(connData->post->buff, "essid", essid, sizeof(essid));
	httpdFindArg(connData->post->buff, "passwd", password, sizeof(password));

  os_printf("essid: %s\npass: %s\n", essid, password);

  strncpy(wifiConfig->essid, essid, 32);
  strncpy(wifiConfig->password, password, 64);

  eeprom_write_config(wifiConfig);

  os_printf("write config complete, setting reset timer\n");

  os_timer_disarm(&system_reset_timer);
  os_timer_setfn(&system_reset_timer, system_restart, NULL);
  os_timer_arm(&system_reset_timer, 1000, 0);

	char buff[32] = "{\"status\": \"OK\"}";
  httpdSend(connData, buff, 32);

  return HTTPD_CGI_DONE;
}

int ICACHE_FLASH_ATTR cgiPing(HttpdConnData *connData) {
	char buff[32] = "pingBack({\"status\": \"OK\"});";
  httpdSend(connData, buff, -1);

  return HTTPD_CGI_DONE;
}

int ICACHE_FLASH_ATTR tplCounter(HttpdConnData *connData, char *token, void **arg) {
	char buff[128];
	if (token==NULL) return HTTPD_CGI_DONE;

	if (os_strcmp(token, "counter")==0) {
		hitCounter++;
		os_printf("%ld", hitCounter);
	}
	httpdSend(connData, buff, -1);
	return HTTPD_CGI_DONE;
}

int ICACHE_FLASH_ATTR tplLocalWlan(HttpdConnData *connData, char *token, void **arg) {
	char buff[1024];
	int x;
	static struct station_config stconf;
	if (token==NULL) return HTTPD_CGI_DONE;
	wifi_station_get_config(&stconf);

	strcpy(buff, "Unknown");
	if (strcmp(token, "WiFiMode")==0) {
		x=wifi_get_opmode();
		if (x==1) strcpy(buff, "Client");
		if (x==2) strcpy(buff, "SoftAP");
		if (x==3) strcpy(buff, "STA+AP");
  } else if (strcmp(token, "hostname")==0) {
    strcpy(buff, (char*)hostname);
  } else if (strcmp(token, "currSsid")==0 && strlen((char*)stconf.ssid)>0) {
    strcpy(buff, (char*)stconf.ssid);
  } else if (strcmp(token, "WiFiPasswd")==0 && strlen((char*)stconf.password)>0) {
    strcpy(buff, (char*)stconf.password);
	} else if (strcmp(token, "WiFiapwarn")==0) {
		x=wifi_get_opmode();
		if (x==2) {
			strcpy(buff, "<b>Can't scan in this mode.</b> Click <a href=\"setmode.cgi?mode=3\">here</a> to go to STA+AP mode.");
		} else {
			strcpy(buff, "Click <a href=\"setmode.cgi?mode=2\">here</a> to go to standalone AP mode.");
		}
	}
	httpdSend(connData, buff, -1);
	return HTTPD_CGI_DONE;
}

int ICACHE_FLASH_ATTR cgiFlashFilesystem(HttpdConnData *connData) {
  int len;
	uint32_t address;
	CgiUploadFlashDef *def=(CgiUploadFlashDef*)connData->cgiArg;

  if(connData->conn == NULL) {
    return HTTPD_CGI_DONE;
  }

  if(connData->requestType != HTTPD_METHOD_POST) {
    httpdStartResponse(connData, 406);
    httpdEndHeaders(connData);

    return HTTPD_CGI_DONE;
  };

	int offset = connData->post->received - connData->post->buffLen;
  if(offset == 0) {
    connData->cgiData = NULL;
  } else if(connData->cgiData != NULL) {
    return HTTPD_CGI_DONE;
  }

  char *err = NULL;
  int code = 400;

	os_printf("Max img sz 0x%X, post len 0x%X\n", def->fwSize, connData->post->len);

  if(connData->post == NULL) err = "No POST request.";
  if(def == NULL) err = "Flash def = NULL ?";

  if(err == NULL && connData->post->len > def->fwSize) err = "Firmware image too large";

	// make sure we're buffering in 1024 byte chunks
	if (err == NULL && offset % 1024 != 0) {
		err = "Buffering problem";
		code = 500;
	}

	// return an error if there is one
	if (err != NULL) {
		httpd_printf("Error %d: %s\n", code, err);
		httpdStartResponse(connData, code);
		httpdHeader(connData, "Content-Type", "text/plain");
		httpdEndHeaders(connData);
		httpdSend(connData, "Firmware image error:\r\n", -1);
		httpdSend(connData, err, -1);
		httpdSend(connData, "\r\n", -1);
		connData->cgiData = (void *)1;
		return HTTPD_CGI_DONE;
	}

  address = def->fw1Pos + offset;

	// erase next flash block if necessary
	if (address % SPI_FLASH_SEC_SIZE == 0){
		os_printf("Erasing flash at 0x%05x\n", (unsigned int)address);
		// We need to erase this block
		spi_flash_erase_sector(address/SPI_FLASH_SEC_SIZE);
	}

	os_printf("Writing %d bytes at 0x%05x (%d of %d)\n", connData->post->buffSize, (unsigned int)address,
			connData->post->received, connData->post->len);

	// Write the data
	spi_flash_write(address, (uint32 *)connData->post->buff, connData->post->buffLen);

	if (connData->post->received == connData->post->len){
		httpdStartResponse(connData, 200);
		httpdEndHeaders(connData);
		return HTTPD_CGI_DONE;
	} else {
		return HTTPD_CGI_MORE;
	}
}

static struct espconn *tcpconn;

void ICACHE_FLASH_ATTR handle_tcp_recv(void *arg, char *pdata, unsigned short len) {
  struct espconn *conn = (struct espconn *) arg;
}

void ICACHE_FLASH_ATTR handle_tcp_connect(void *arg) {
  struct espconn *conn = (struct espconn *) arg;

  os_printf("TCP connection.\n");
  struct HttpdConnData *httpConn = (HttpdConnData *)conn->reverse;

  char req[256];
  os_printf("GET %s\n\n", httpConn->url);
}

int ICACHE_FLASH_ATTR cgiProxyRequest(HttpdConnData *connData) {
  tcpconn = (struct espconn*)os_zalloc(sizeof(struct espconn));

  if(connData->conn == NULL) {
    return HTTPD_CGI_DONE;
  }

  tcpconn->type = ESPCONN_TCP;
  tcpconn->state = ESPCONN_NONE;
  tcpconn->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
  tcpconn->proto.tcp->local_port = espconn_port();
  tcpconn->proto.tcp->remote_port = 80;

  tcpconn->proto.tcp->remote_ip[0] = 192;
  tcpconn->proto.tcp->remote_ip[1] = 168;
  tcpconn->proto.tcp->remote_ip[2] = 193;
  tcpconn->proto.tcp->remote_ip[3] = 223;

  tcpconn->reverse = connData;

  espconn_regist_connectcb(tcpconn, handle_tcp_connect);
  espconn_regist_recvcb(tcpconn, handle_tcp_recv);

  espconn_connect(tcpconn);
}

CgiUploadFlashDef uploadParams={
  .type=CGIFLASH_TYPE_ESPFS,
  .fw1Pos=ESPFS_POS,
  .fw2Pos=0,
  .fwSize=ESPFS_SIZE,
};


HttpdBuiltInUrl builtInUrls[]={
	{"/", cgiRedirect, "/index.tpl"},
	{"/index.tpl", cgiEspFsTemplate, tplCounter},

	{"/wifi", cgiRedirect, "/wifi/wifi.tpl"},
	{"/wifi/", cgiRedirect, "/wifi/wifi.tpl"},
  {"/wifi/connect.cgi", cgiWiFiConnectHandler, NULL},
	{"/wifi/wifiscan.cgi", cgiWiFiScan, NULL},
	{"/wifi/wifi.tpl", cgiEspFsTemplate, tplLocalWlan},

	{"*", cgiEspFsHook, NULL}, //Catch-all cgi function for the filesystem

	{NULL, NULL, NULL}
};

bool ICACHE_FLASH_ATTR config_http_server() {
	captdnsInit();

	espFsInit((void*)(0x40200000 + ESPFS_POS));
	httpdInit(builtInUrls, 80);
}
