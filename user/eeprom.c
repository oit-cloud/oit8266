#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <mem.h>
#include <spi_flash.h>
#include "eeprom.h"

#define CONFIG_ADDR 0x3E * SPI_FLASH_SEC_SIZE

int ICACHE_FLASH_ATTR eeprom_write_config(WifiConfig *wifiConfig) {
  spi_flash_erase_sector(CONFIG_ADDR / SPI_FLASH_SEC_SIZE);
  spi_flash_write(CONFIG_ADDR, (uint32 *)wifiConfig, sizeof(WifiConfig));
};

int ICACHE_FLASH_ATTR eeprom_read_config(WifiConfig *wifiConfig) {
  spi_flash_read((uint32)CONFIG_ADDR, (uint32 *)wifiConfig, sizeof(WifiConfig));
}
