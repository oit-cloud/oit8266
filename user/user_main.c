/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_main.c
 *
 * Description: entry file of user application
 *
 * Modification history:
 *     2014/1/1, v1.0 create this file.
*******************************************************************************/
#include <c_types.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <mem.h>
#include <gpio.h>
#include <oit.h>
#include "http.h"
#include "eeprom.h"

enum DeviceTypes {
  Switch,
  Fader
};

struct softap_config ap_config;
struct station_config station_config;

#define WIFI_AP_SSID "ESP_OIT_"

char *hostname;

static int led = 12;
static int led_state = 0;

void ProcessCommand(char* str) {
  
}

void wifi_handle_event_cb(System_Event_t *evt) {
  switch(evt->event) {
    case EVENT_STAMODE_CONNECTED:
      os_printf("EVENT_STAMODE_CONNECTED\nESSID: %s\nChannel: %d\n\n",
        evt->event_info.connected.ssid,
        evt->event_info.connected.channel
      ); 

      break;

    case EVENT_STAMODE_DISCONNECTED:
      os_sprintf("EVENT_STAMODE_DISCONNECTED\nESSID: %s\n Reason: %d\n\n",
        evt->event_info.disconnected.ssid,
        evt->event_info.disconnected.reason
      );

      break;

    case EVENT_STAMODE_AUTHMODE_CHANGE:
      os_printf("EVENT_STAMODE_AUTHMODE_CHANGE\n");
      break;

    case EVENT_STAMODE_GOT_IP:
      os_printf("EVENT_STAMODE_GOT_IP\nIP: " IPSTR "\nMask: " IPSTR "\nGateway: " IPSTR "\n\n",
        IP2STR(&evt->event_info.got_ip.ip),
        IP2STR(&evt->event_info.got_ip.mask),
        IP2STR(&evt->event_info.got_ip.gw)
      );
      break;

    case EVENT_SOFTAPMODE_STACONNECTED:
      os_printf("EVENT_SOFTAPMODE_STACONNECTED\n");
      break;

    case EVENT_SOFTAPMODE_STADISCONNECTED:
      os_printf("EVENT_SOFTAPMODE_STADISCONNECTED\n");
      break;

    case EVENT_SOFTAPMODE_PROBEREQRECVED:
      //os_printf("EVENT_SOFTAPMODE_PROBEREQRECVED\n");
      break;
  }

}

void config_wifi_station(WifiConfig *wifiConfig) {
  os_strcpy(&station_config.ssid, wifiConfig->essid, os_strlen(station_config.ssid));
  os_strcpy(&station_config.password, wifiConfig->password, os_strlen(station_config.password));

  //station_config.bssid_set = 0;

  wifi_station_set_hostname(hostname);
  wifi_station_set_config(&station_config);
}

void config_wifi_ap() {
  os_memset(&ap_config, 0, sizeof(struct softap_config));
  char *ESSID = (char *)os_zalloc(sizeof(WIFI_AP_SSID) + 7);
  os_sprintf(ESSID, WIFI_AP_SSID "%06x", spi_flash_get_id());

  os_strcpy(&ap_config.ssid, ESSID, os_strlen(ESSID));
  ap_config.authmode = AUTH_OPEN;
  ap_config.max_connection = 2;

  wifi_set_opmode(STATIONAP_MODE);
  wifi_softap_dhcps_stop();

  wifi_softap_set_config(&ap_config);
  wifi_set_event_handler_cb(wifi_handle_event_cb); 

  struct ip_info info;
  IP4_ADDR(&info.ip, 192, 168, 100, 1);
  IP4_ADDR(&info.gw, 192, 168, 100, 1);
  IP4_ADDR(&info.netmask, 255, 255, 255, 0);
  wifi_set_ip_info(SOFTAP_IF, &info);

  wifi_softap_dhcps_start();

  if (wifi_get_ip_info(SOFTAP_IF, &info)) {
    os_printf("Station IP   : " IPSTR "\n", IP2STR(&(info.ip)));
    os_printf("Station GW   : " IPSTR "\n", IP2STR(&(info.gw)));
    os_printf("Station Mask : " IPSTR "\n\n", IP2STR(&(info.netmask)));
  }

  os_free(info);
  os_free(ap_config);
}

void user_rf_pre_init(void)
{
  system_phy_set_rfoption(2);
}

OitHandler oitHandlers[]={
  {OIT_TYPE_GPIO, oitGpioToggleGetCB, oitGpioToggleSetCB, 12, NULL },
  {OIT_TYPE_PWM, oitPwmGetCB, oitPwmSetCB, 12, __INT_MAX__ / 2 },
  { NULL }
};

void user_init(void) {
  gpio_init();

  hostname = (char *)os_zalloc(sizeof(uint32) + 5);
  os_printf("oit%06x\0", spi_flash_get_id());
  os_printf("\r\n\r\nSDK version:%s\nHostname: %s\n\n", system_get_sdk_version(), hostname);

  wifi_set_broadcast_if(STATIONAP_MODE);

  os_printf("Configuring wifi AP.\n");
  config_wifi_ap();

  os_printf("Configuring HTTP server.\n\n");
  config_http_server();

  PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);
  GPIO_OUTPUT_SET(led, led_state);

  WifiConfig *wifiConfig = (WifiConfig*)os_zalloc(sizeof(WifiConfig));
  eeprom_read_config(wifiConfig);

  if(strlen(wifiConfig->essid) > 0) {
    os_printf("Got saved WiFi config\nESSID: %s\nPassword: %s\n\n", wifiConfig->essid, wifiConfig->password);

    os_printf("Configuring wifi client.\n");
    config_wifi_station(wifiConfig);
  }

  oit_init(oitHandlers);
}
