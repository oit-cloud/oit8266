<html>
  <head>
    <title>Esp8266 web server</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>

  <body>
    <div id="main">
      <h1>Oit Cloud</h1>

      <a href="/wifi">Setup WiFi!</a>
      <div>
        Web server powered by <a href="https://github.com/Spritetm/libesphttpd">libesphttpd</a> by @<a href="https://twitter.com/SpritesMods">SpritesMods</a>
      </div>

      <div>
        <p>And because we're on the Internets now, here are the required pictures of.... A HAMSTER!</p>
        <img alt="Hamala the hamster!" src="img/ham.jpg" />
      </div>

      <div>
        Page loaded <b>%counter%</b> times.
      </div>
    </div>
  </body>
</html>
